package com.hanidan.moodscanner.TextFonts.ButtonTextFont;


import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class ButtonDeftone extends androidx.appcompat.widget.AppCompatButton {
    public ButtonDeftone(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/deftone stylus.ttf"));
    }
}
