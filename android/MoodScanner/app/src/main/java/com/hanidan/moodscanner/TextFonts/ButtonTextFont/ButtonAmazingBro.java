package com.hanidan.moodscanner.TextFonts.ButtonTextFont;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

public class ButtonAmazingBro extends Button {
    public ButtonAmazingBro(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/amazing.ttf"));
    }
}
