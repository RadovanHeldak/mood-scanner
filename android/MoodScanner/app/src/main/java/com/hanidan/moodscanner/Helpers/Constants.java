package com.hanidan.moodscanner.Helpers;

public class Constants {
    public static final int SCANNING_INTERVAL = 3900;
    public static final String APP_URL = "https://play.google.com/store/apps/details?id=com.hanidan.moodscanner";
    public static final int REQUEST_WRITE_STORAGE = 112;
}
