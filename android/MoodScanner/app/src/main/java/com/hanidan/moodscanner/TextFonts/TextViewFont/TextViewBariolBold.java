package com.hanidan.moodscanner.TextFonts.TextViewFont;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class TextViewBariolBold extends androidx.appcompat.widget.AppCompatTextView {
    public TextViewBariolBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(),"fonts/Roboto-Regular.ttf"));
    }
}
