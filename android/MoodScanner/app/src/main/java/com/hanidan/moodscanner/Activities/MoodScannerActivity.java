package com.hanidan.moodscanner.Activities;

import static com.hanidan.moodscanner.Helpers.Constants.APP_URL;
import static com.hanidan.moodscanner.Helpers.Constants.REQUEST_WRITE_STORAGE;
import static com.hanidan.moodscanner.Helpers.Constants.SCANNING_INTERVAL;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.FragmentManager;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.google.android.ump.ConsentInformation;
import com.google.android.ump.ConsentRequestParameters;
import com.google.android.ump.UserMessagingPlatform;
import com.hanidan.moodscanner.BuildConfig;
import com.hanidan.moodscanner.Enum.Mood;
import com.hanidan.moodscanner.Fragments.DialogShareFragment;
import com.hanidan.moodscanner.Helpers.PhoneState;
import com.hanidan.moodscanner.R;
import com.hanidan.moodscanner.TextFonts.TextViewFont.TextViewAmazingBro;
import com.mobapphome.mahads.MAHAdsController;
import com.mobapphome.mahads.MAHAdsDlgExit;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;

public class MoodScannerActivity extends AppCompatActivity implements MAHAdsDlgExit.MAHAdsDlgExitListener {

    private static final String TAG = MoodScannerActivity.class.getSimpleName();

    private InterstitialAd mInterstitialAd = null;

    private ImageView smileImageView, longResult;
    private AnimationSet lightLineAnimation;
    private RelativeLayout fingerprintLayout, scanningLayout, resultLayout,
            scanningThreeDotsLayout, moodResult, mainLayout, lightLineLayout, open_share_dialog,
            shareButtonsLayout;
    private TextView loadedPercentageTextView, moodText, scanningText, scanningThreeDots,
            theResultText;
    private Timer loadingPercentageTimer;
    private Button shareButton, tryAgainButton, showResultButton;
    private TextViewAmazingBro shareText;

    private MAHAdsController mahAdsController;

    private int loadedPercentage;
    private boolean isFingerDown;
    private boolean analyzing;

    private int height, width;

    private boolean isSharing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;

        initGDPRConsent();

        if (!checkPermission()) {
            Log.d(TAG, "onCreate: permission not granted");
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_WRITE_STORAGE);
        }

        mahAdsController = MAHAdsController.getInstance();
        mahAdsController.init(this, savedInstanceState, "https://onepixel.studio/app_promotion/",
                "mood_scanner/program_version.json", "mood_scanner/program_list.json");
        init();
    }

    private void loadInterstitialAd() {
        String adUnitId = getResources().getString(BuildConfig.DEBUG ? R.string.interstitial_ad_test : R.string.interstitial_ad_id);

        InterstitialAd.load(this, adUnitId, new AdRequest.Builder().build(),
                new InterstitialAdLoadCallback() {
                    @Override
                    public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                        mInterstitialAd = interstitialAd;
                    }
                });
    }

    private void showInterstitialAd() {
        if (mInterstitialAd != null) {
            mInterstitialAd.show(this);
            mInterstitialAd = null;
        }
        loadInterstitialAd();
    }

    private boolean checkPermission() {
        return true;//(ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void init() {
        loadedPercentageTextView = findViewById(R.id.loaded_percentage_text_view);
        fingerprintLayout = findViewById(R.id.fingerprint_layout);
        scanningLayout = findViewById(R.id.scanning_layout);
        resultLayout = findViewById(R.id.result_layout);
        moodText = findViewById(R.id.mood_text);
        scanningThreeDots = findViewById(R.id.scanning_three_dots);
        scanningThreeDotsLayout = findViewById(R.id.scanning_three_dots_layout);
        smileImageView = findViewById(R.id.smile_result);
        moodResult = findViewById(R.id.mood_frame_result);
        longResult = findViewById(R.id.long_result);
        showResultButton = findViewById(R.id.show_result_button);
        shareButton = findViewById(R.id.share_button);
        tryAgainButton = findViewById(R.id.try_again_button);
        mainLayout = findViewById(R.id.main_layout);
        scanningText = findViewById(R.id.place_finger_text);
        theResultText = findViewById(R.id.the_result_text);
        lightLineLayout = findViewById(R.id.light_line_layout);
        open_share_dialog = findViewById(R.id.open_share_dialog);
        shareText = findViewById(R.id.share);
        shareButtonsLayout = findViewById(R.id.share_buttons_layout);


        scanningText.setText(getString(R.string.place_finger_text));

        ((RelativeLayout.LayoutParams) shareButton.getLayoutParams()).topMargin = height * 508 / 1000 - shareButton.getLayoutParams().height / 2;
        ((RelativeLayout.LayoutParams) tryAgainButton.getLayoutParams()).topMargin = height * 628 / 1000 - tryAgainButton.getLayoutParams().height / 2;
        ((RelativeLayout.LayoutParams) moodResult.getLayoutParams()).topMargin = height * 320 / 1000 - moodResult.getLayoutParams().height / 2;
        ((RelativeLayout.LayoutParams) theResultText.getLayoutParams()).topMargin = height * 122 / 1000 - scanningText.getLayoutParams().height / 2;
        ((RelativeLayout.LayoutParams) scanningText.getLayoutParams()).topMargin = ((RelativeLayout.LayoutParams) theResultText.getLayoutParams()).topMargin;
        ((RelativeLayout.LayoutParams) shareText.getLayoutParams()).topMargin = height * 744 / 1000 - shareText.getLayoutParams().height / 2;
        ((RelativeLayout.LayoutParams) shareButtonsLayout.getLayoutParams()).topMargin = height * 795 / 1000 - shareButtonsLayout.getLayoutParams().height / 2;

        fingerprintLayout.setClipToOutline(true);

        fingerprintLayout.setOnTouchListener((v, event) -> {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN: {
                    if (!analyzing) {
                        loadedPercentage = 0;
                        isFingerDown = true;
                        lightLineLayout.startAnimation(lightLineAnimation);
                        scanningText.setText(getString(R.string.scanning));
                        loadedPercentageTextView.setVisibility(View.VISIBLE);
                        scanningThreeDotsLayout.setVisibility(View.VISIBLE);
                        startLoadingPercentageTimer();
                    }
                    break;
                }
                case MotionEvent.ACTION_UP: {
                    isFingerDown = false;
                    if (!analyzing) {
                        loadedPercentageTextView.setVisibility(View.GONE);
                        scanningText.setText(getString(R.string.place_finger_text));
                        lightLineLayout.clearAnimation();
                    }
                    break;
                }
            }
            return true;
        });

        lightLineAnimation = (AnimationSet) AnimationUtils.loadAnimation(this, R.anim.line_up);

        lightLineAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (isFingerDown && !analyzing) {
                    lightLineLayout.startAnimation(lightLineAnimation);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        open_share_dialog.setOnClickListener(view -> {

            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_TEXT, "Mood Scanner - Android Apps on Google Play\n" + APP_URL);
            startActivity(Intent.createChooser(shareIntent, "Share link using"));

            /*FragmentManager fm = getSupportFragmentManager();

            DialogShareFragment dialogShareFragment = new DialogShareFragment();

            dialogShareFragment.show(fm, "Share Dialog Fragment");*/
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void startLoadingPercentageTimer() {
        loadingPercentageTimer = new Timer();
        loadingPercentageTimer.schedule(new LoadingPercentageTimerTask(), SCANNING_INTERVAL / 100);
    }

    private void showResult() {
        runOnUiThread(() -> {
            loadedPercentageTextView.setVisibility(View.GONE);
            scanningText.setText(getString(R.string.place_finger_text));
            lightLineLayout.clearAnimation();
            scanningLayout.setVisibility(View.GONE);
            resultLayout.setVisibility(View.VISIBLE);
            Mood mood = Mood.getRandomMood();
            moodText.setText(mood.getMoodTitle(MoodScannerActivity.this));
            smileImageView.setImageResource(mood.getMoodSmile());
            //showInterstitialAd();
        });
    }

    private void hideResult() {
        updatePercentage(0);
        runOnUiThread(() -> {
            resultLayout.setVisibility(View.GONE);
            showResultButton.setVisibility(View.GONE);
            fingerprintLayout.setVisibility(View.VISIBLE);
            scanningLayout.setVisibility(View.VISIBLE);
        });
    }

    @Override
    public void onYes() {

    }

    @Override
    public void onNo() {

    }

    @Override
    public void onExitWithoutExitDlg() {

    }

    @Override
    public void onEventHappened(String eventStr) {

    }

    private class LoadingPercentageTimerTask extends TimerTask {
        @Override
        public void run() {
            loadedPercentage++;
            loadingPercentageTimer.cancel();
            loadingPercentageTimer.purge();
            if (loadedPercentage < 100 && (isFingerDown || analyzing)) {
                startLoadingPercentageTimer();
            } else if (loadedPercentage == 100) {
                if (analyzing) {
                    runOnUiThread(() -> {
                        showResultButton.setVisibility(View.VISIBLE);
                        fingerprintLayout.setVisibility(View.INVISIBLE);
                    });
                    analyzing = false;
                } else {
                    updatePercentage(0);
                    analyzing = true;
                    runOnUiThread(() -> {
                        lightLineLayout.clearAnimation();
                        scanningText.setText(getString(R.string.analyzing));
                    });
                    startLoadingPercentageTimer();
                }
            } else {
                loadedPercentage = 0;
            }
            updatePercentage();

        }
    }

    private void updatePercentage(int percentage) {
        loadedPercentage = percentage;
        updatePercentage();
    }

    private void updatePercentage() {
        runOnUiThread(() -> {
            String loadedPercentageText = "" + (analyzing ? loadedPercentage / 10 * 10 : loadedPercentage) + " %";
            loadedPercentageTextView.setText(loadedPercentageText);
            /*if (loadedPercentage > 0 && loadedPercentage < 100) {

                if (loadedPercentage % 10 == 0) {
                    StringBuilder text = new StringBuilder();
                    for (int i = 0; i <= loadedPercentage / 10 % 3; i++) {
                        text.append(".");
                    }

                    scanningThreeDots.setText(text);
                }

            }*/
        });
    }

    public void onShowResultClick(View v) {
        showInterstitialAd();
        showResult();
    }

    public void onTryAgainClick(View v) {
        showInterstitialAd();
        hideResult();
    }

    public void onShareClick(View v) {
        openShareDialog();
        /*if (checkPermission()) {
            openShareDialog();
        } else {
            isSharing = true;
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_WRITE_STORAGE);
        }*/
    }

    private void openShareDialog() {
        //saveResult();

        Drawable drawable = ContextCompat.getDrawable(this, R.drawable.blue_background);

        moodResult.setBackground(drawable);

        longResult.setImageResource(R.drawable.long_result_with_back);

        shareResult();

        longResult.setImageResource(R.drawable.long_result);

        moodResult.setBackground(null);

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mahAdsController.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        if (scanningLayout.getVisibility() != View.GONE) {
            if (PhoneState.isNetworkAvailable(this)) {
                mahAdsController.callProgramsDialog(this);
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i("result", "onActivityResult: " + requestCode);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_WRITE_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (isSharing) {
                        openShareDialog();
                    }
                }
            }
        }

    }

    public static Bitmap viewToBitmap(View view, int width, int height) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }

    public void shareResult() {
        Bitmap bitmap = viewToBitmap(moodResult, moodResult.getWidth(), moodResult.getHeight());
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("image/jpeg");
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(CompressFormat.JPEG, 100, byteArrayOutputStream);
        File file = new File(getFilesDir(), "MoodScanner.jpg");
        try {
            file.createNewFile();
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(byteArrayOutputStream.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }
        //shareIntent.putExtra(Intent.EXTRA_TEXT, "Discovered via MoodScanner for Android. Get it at " + APP_URL);

        shareIntent.putExtra(Intent.EXTRA_STREAM, FileProvider.getUriForFile(this, getString(R.string.file_provider_authority), file));
        shareIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(Intent.createChooser(shareIntent, "Share Image"));
    }

    public void saveResult() {
        FileOutputStream fileOutputStream;
        File file = getDisc();
        if (!file.exists() && !file.mkdirs()) {
            Toast.makeText(this, "Can't create directory to save image", Toast.LENGTH_SHORT).show();
            return;
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyymmsshhmmss", Locale.getDefault());
        String date = simpleDateFormat.format(new Date());
        String name = "Img" + date + ".jpg";
        String file_name = file.getAbsolutePath() + "/" + name;
        File new_file = new File(file_name);
        try {
            fileOutputStream = new FileOutputStream(file_name);
            Bitmap bitmap = viewToBitmap(moodResult, moodResult.getWidth(), moodResult.getHeight());
            bitmap.compress(CompressFormat.JPEG, 100, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        refreshGallery(new_file);

    }

    private void refreshGallery(File file) {
        Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        intent.setData(Uri.fromFile(file));
        sendBroadcast(intent);
    }

    private File getDisc() {
        File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        return new File(file, "Mood Scanner");
    }

    private final AtomicBoolean isMobileAdsInitializeCalled = new AtomicBoolean(false);

    private ConsentInformation consentInformation;

    private void initGDPRConsent() {
        ProgressDialog consentProgressDialog = new ProgressDialog(this, R.style.ConsentProgressDialogStyle);
        consentProgressDialog.setCancelable(false);
        consentProgressDialog.setMessage("Loading, please wait...");
        consentProgressDialog.show();


        ConsentRequestParameters params = new ConsentRequestParameters
                .Builder()
                .setTagForUnderAgeOfConsent(false)
                .build();

        consentInformation = UserMessagingPlatform.getConsentInformation(this);
        consentInformation.requestConsentInfoUpdate(this, params,
                () -> {
                    Log.e(TAG, "show consent");
                    UserMessagingPlatform.loadAndShowConsentFormIfRequired(
                            this,
                            loadAndShowError -> {
                                Log.e(TAG, "consent load");
                                consentProgressDialog.dismiss();
                                if (loadAndShowError != null) {
                                    // Consent gathering failed.
                                    Log.w(TAG, String.format("%s: %s",
                                            loadAndShowError.getErrorCode(),
                                            loadAndShowError.getMessage()));
                                }

                                if (consentInformation.canRequestAds()) {
                                    initializeMobileAdsSdk();
                                }

                            }
                    );
                },
                requestConsentError -> {
                    consentProgressDialog.dismiss();
                    initializeMobileAdsSdk();
                    Log.w(TAG, String.format("%s: %s",
                            requestConsentError.getErrorCode(),
                            requestConsentError.getMessage()));
                });

        if (consentInformation.canRequestAds()) {
            consentProgressDialog.dismiss();
            initializeMobileAdsSdk();
        }

    }

    private void initializeMobileAdsSdk() {
        if (isMobileAdsInitializeCalled.getAndSet(true)) {
            return;
        }

        MobileAds.initialize(this, initializationStatus -> {
            loadInterstitialAd();
        });
    }

}
