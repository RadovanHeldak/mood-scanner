package com.hanidan.moodscanner.TextFonts.TextViewFont;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;


public class TextViewBariolLight extends androidx.appcompat.widget.AppCompatTextView {
    public TextViewBariolLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(),"fonts/zekton_rg.ttf"));
    }
}
