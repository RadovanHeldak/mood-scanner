package com.hanidan.moodscanner.Fragments;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.viewpager.widget.ViewPager.LayoutParams;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


import com.hanidan.moodscanner.R;

import java.net.URL;

import static com.hanidan.moodscanner.Helpers.Constants.APP_URL;

public class DialogShareFragment extends DialogFragment {

    private int height;
    private ImageView share_button_all;
    private Button cancel_share_button;
    private TextView share_text;

    private URL url;

    private Window window;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        window = getDialog().getWindow();
        window.requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(false);

        View rootView = inflater.inflate(R.layout.dialog_fragment_layout, container, false);
        init(rootView);
        onClickMethods();

        height = getResources().getDimensionPixelSize(R.dimen.dialog_height);

        return rootView;
    }

    private void onClickMethods() {
        share_button_all.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                getDialog().dismiss();
                hideDialogFragment();

                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, "Mood Scanner - Android Apps on Google Play \n #moodscanner \n" + APP_URL);
                startActivity(Intent.createChooser(shareIntent, "Share link using"));

            }
        });
        cancel_share_button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                getDialog().dismiss();

                hideDialogFragment();

            }
        });
    }

    public void hideDialogFragment() {

        DialogShareFragment dialogShareFragment = new DialogShareFragment();

        getActivity().getSupportFragmentManager().beginTransaction().hide(dialogShareFragment).commit();

    }

    private void init(View view) {
        cancel_share_button = view.findViewById(R.id.cancel_share_button);
        share_button_all = view.findViewById(R.id.share_button_all);
        share_text = view.findViewById(R.id.share_text);
        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Regular.ttf");
        Typeface font2 = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Medium.ttf");
        cancel_share_button.setTypeface(font);
        share_text.setTypeface(font2);

    }

    public void onResume() {
        super.onResume();
        window.setLayout(LayoutParams.MATCH_PARENT, height);
        window.setGravity(Gravity.CENTER);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}