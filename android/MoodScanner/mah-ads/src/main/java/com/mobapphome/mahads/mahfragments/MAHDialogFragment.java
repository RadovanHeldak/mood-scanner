package com.mobapphome.mahads.mahfragments;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;

/**
 * Created by settar on 12/20/16.
 */


public class MAHDialogFragment extends DialogFragment {

    public FragmentActivity getActivityMAH() throws MAHFragmentExeption {
        if (getActivity() != null) {
            return getActivity();
        } else {
            throw new MAHFragmentExeption("MAHFragment getActivity() method returns null");
        }
    }
}