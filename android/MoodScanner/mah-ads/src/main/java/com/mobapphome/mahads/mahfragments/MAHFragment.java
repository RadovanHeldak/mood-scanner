package com.mobapphome.mahads.mahfragments;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;


public class MAHFragment extends Fragment {


    public FragmentActivity getActivityMAH() throws MAHFragmentExeption {
        if (getActivity() != null) {
            return getActivity();
        } else {
            throw new MAHFragmentExeption("MAHFragment getActivity() method returns null");
        }
    }

}
