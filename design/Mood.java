package com.hanidan.moodscanner.Enum;

import android.content.Context;

import com.hanidan.moodscanner.R;

import java.util.Random;

public enum Mood {

    AGGRAVATED(0, R.string.aggravated, R.drawable.ic_smile11),
    AMUSED(1, R.string.amused, R.drawable.ic_smile8),
    ANGRY(2, R.string.angry, R.drawable.ic_smile11),
    ANNOYED(3, R.string.annoyed, R.drawable.ic_smile12),
    ANXIOUS(4, R.string.anxious, R.drawable.ic_smile22),
    APATHETIC(5, R.string.apathetic, R.drawable.ic_smile7),
    BEWILDERED(6, R.string.bewildered, R.drawable.ic_smile18),
    BLISSFUL(7, R.string.blissful, R.drawable.ic_smile17),
    BOUNCY(8, R.string.bouncy, R.drawable.ic_smile23),
    CALM(9, R.string.calm, R.drawable.ic_smile27),
    CHEERFUL(10, R.string.cheerful, R.drawable.ic_smile3),
    COMPLACENT(11, R.string.complacent, R.drawable.ic_smile3),
    CONTENT(12, R.string.content, R.drawable.ic_smile28),
    CRANKY(13, R.string.cranky, R.drawable.ic_smile24),
    CRAPPY(14, R.string.crappy, R.drawable.ic_smile12),
    CRAZY(15, R.string.crazy, R.drawable.ic_smile2),
    CURIOUS(16, R.string.curious, R.drawable.ic_smile26),
    CYNICAL(17, R.string.cynical, R.drawable.ic_smile9),
    DEPRESSED(18, R.string.depressed, R.drawable.ic_smile21),
    DEVIOUS(19, R.string.devious, R.drawable.ic_smile20),
    DISAPPOINTED(20, R.string.disappointed, R.drawable.ic_smile9),
    DISCONTENT(21, R.string.discontent, R.drawable.ic_smile11),
    DITZY(22, R.string.ditzy, R.drawable.ic_smile6),
    DREAMY(23, R.string.dreamy, R.drawable.ic_smile10),
    ECSTATIC(24, R.string.ecstatic, R.drawable.ic_smile1),
    ENERGETIC(25, R.string.energetic, R.drawable.ic_smile19),
    ENRAGED(26, R.string.enraged, R.drawable.ic_smile11),
    ENTHRALLED(27, R.string.enthralled, R.drawable.ic_smile26),
    ENVIOUS(28, R.string.envious, R.drawable.ic_smile9),
    EXCITED(29, R.string.excited, R.drawable.ic_smile28),
    FLIRTY(30, R.string.flirty, R.drawable.ic_smile14),
    FRUSTRATED(31, R.string.frustrated, R.drawable.ic_smile21),
    GIDDY(32, R.string.giddy, R.drawable.ic_smile18),
    GIGGLY(33, R.string.giggly, R.drawable.ic_smile29),
    GLOOMY(34, R.string.gloomy, R.drawable.ic_smile20),
    GOOD(35, R.string.good, R.drawable.ic_smile15),
    GRATEFUL(36, R.string.grateful, R.drawable.ic_smile3),
    GRUMPY(37, R.string.grumpy, R.drawable.ic_smile7),
    GUILTY(38, R.string.guilty, R.drawable.ic_smile18),
    HAPPY(39, R.string.happy, R.drawable.ic_smile2),
    HIGH(40, R.string.high, R.drawable.ic_smile19),
    HOPEFUL(41, R.string.hopeful, R.drawable.ic_smile4),
    IMPRESSED(42, R.string.impressed, R.drawable.ic_smile28),
    INDIFFERENT(43, R.string.indifferent, R.drawable.ic_smile27),
    INFURIATED(44, R.string.infuriated, R.drawable.ic_smile11),
    INSPIRED(45, R.string.inspired, R.drawable.ic_smile30),
    IRATE(46, R.string.irate, R.drawable.ic_smile11),
    IRRITATED(47, R.string.irritated, R.drawable.ic_smile12),
    JOYFUL(48, R.string.joyful, R.drawable.ic_smile8),
    JUBILANT(49, R.string.jubilant, R.drawable.ic_smile8),
    LAZY(50, R.string.lazy, R.drawable.ic_smile6),
    LETHARGIC(51, R.string.lethargic, R.drawable.ic_smile27),
    LISTLESS(52, R.string.listless, R.drawable.ic_smile25),
    LONELY(53, R.string.lonely, R.drawable.ic_smile21),
    LOVING(54, R.string.loving, R.drawable.ic_smile10),
    MAD(55, R.string.mad, R.drawable.ic_smile29),
    MELANCHOLY(56, R.string.melancholy, R.drawable.ic_smile16),
    MELLOW(57, R.string.mellow, R.drawable.ic_smile15),
    MISCHIEVOUS(58, R.string.mischievous, R.drawable.ic_smile11),
    MOURNFUL(59, R.string.mournful, R.drawable.ic_smile5),
    OPTIMISTIC(60, R.string.optimistic, R.drawable.ic_smile1),
    PEACEFUL(61, R.string.peaceful, R.drawable.ic_smile3),
    PESSIMISTIC(62, R.string.pessimistic, R.drawable.ic_smile24),
    PLEASED(63, R.string.pleased, R.drawable.ic_smile17),
    REJECTED(64, R.string.rejected, R.drawable.ic_smile18),
    REJUVENATED(65, R.string.rejuvenated, R.drawable.ic_smile15),
    RELIEVED(66, R.string.relieved, R.drawable.ic_smile3),
    RESTLESS(67, R.string.restless, R.drawable.ic_smile9),
    SAD(68, R.string.sad, R.drawable.ic_smile21),
    SATISFIED(69, R.string.satisfied, R.drawable.ic_smile17),
    SHOCKED(70, R.string.shocked, R.drawable.ic_smile26),
    SILLY(71, R.string.silly, R.drawable.ic_smile7),
    SMART(72, R.string.smart, R.drawable.ic_smile4),
    STRESSED(73, R.string.stressed, R.drawable.ic_smile22),
    SURPRISED(74, R.string.surprised, R.drawable.ic_smile26),
    SYMPATHETIC(75, R.string.sympathetic, R.drawable.ic_smile4),
    THANKFUL(76, R.string.thankful, R.drawable.ic_smile3),
    WEIRD(77, R.string.weird, R.drawable.ic_smile24);

    private int value;
    private int moodTitle;
    private int moodSmile;

    Mood(int value, int moodTitle, int moodSmile) {
        this.value = value;
        this.moodTitle = moodTitle;
        this.moodSmile = moodSmile;
    }

    public int getValue() {
        return value;
    }

    public String getMoodTitle(Context context){
        return context.getString(moodTitle);
    }

    public int getMoodSmile(){
        return moodSmile;
    }

    public static Mood getRandomMood(){
        int code = new Random().nextInt(Mood.values().length);
        for(Mood m : Mood.values()){
            if(code == m.value) return m;
        }
        return HAPPY;
    }
}
