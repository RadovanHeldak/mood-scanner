//
//  MoodController.swift
//  Mood scanner
//
//  Created by Michal Manak on 01/11/2017.
//  Copyright © 2017 Michal Manak. All rights reserved.
//

import UIKit
import GoogleMobileAds

class MoodController: UIViewController, GADInterstitialDelegate{

    @IBOutlet weak var try_again_button: UIButton!
    @IBOutlet weak var smile: UIImageView!
    @IBOutlet weak var longResult: UIImageView!
    @IBOutlet weak var mood: UILabel!
    @IBOutlet weak var share_button: UIButton!
    @IBOutlet var mainView: UIView!
    
    var interstitial: GADInterstitial!
   
    let s = Smiles()
    var smileName: String?
    var moodName: String?
    
    var timer:Timer?
    
    var width: CGFloat?


    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        try_again_button.layer.cornerRadius = 25
        share_button.layer.cornerRadius = 25
        let (a, b) = s.getRandomSmile()
        smileName = a
        moodName = b
        mood.text = NSLocalizedString(moodName!, comment: "")
        smile.image = UIImage(named: smileName!)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        width = mainView.frame.size.width
        if width! < CGFloat(370){
            longResult.frame.size.width = 320
            longResult.frame.size.height = 200
            longResult.frame.origin.y = 0.29 * mainView.frame.size.height
            longResult.frame.origin.x = 0
            smile.frame.origin.y = 0.29 * mainView.frame.size.height
            smile.frame.origin.x = 0
            smile.frame.size.width = 97
        }
        showAd()
    }
    
    @IBAction func segue_back(_ sender: Any) {
        performSegue(withIdentifier: "unwind", sender: self)
        
    }
 
    @IBAction func share(_ sender: Any) {
        
        let image_temp = takeScreenshot(inView: mainView)
        var image: UIImage?
        if width! < CGFloat(370){
            image = crop(image: image_temp, cropRect: CGRect(x: 0, y:
                image_temp.size.height*0.465 - 70, width: width!, height: 140))
        }else{
            image = crop(image: image_temp, cropRect: CGRect(x: image_temp.size.width/2 - 180, y:
                image_temp.size.height*0.465 - 70, width: 360, height: 140))
        }
        // set up activity view controller
        let imageToShare = [ image! ]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func takeScreenshot(inView: UIView) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(inView.bounds.size, inView.isOpaque, 0.0)
        let context = UIGraphicsGetCurrentContext()
        inView.layer.render(in: context!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    func crop(image:UIImage, cropRect:CGRect) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(cropRect.size, false, image.scale)
        let origin = CGPoint(x: cropRect.origin.x * CGFloat(-1), y: cropRect.origin.y * CGFloat(-1))
        image.draw(at: origin)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        return result
    }
    
    func createAndLoadInterstitial() {
        let r = Int(arc4random_uniform(3))
        if r == 0 {
            interstitial = GADInterstitial(adUnitID: "ca-app-pub-6017537409524741/8205605039")
        }
        if r == 1 {
            interstitial = GADInterstitial(adUnitID: "ca-app-pub-1248704744412699/4122883627")
        }
        if r == 2 {
            interstitial = GADInterstitial(adUnitID: "ca-app-pub-4405272248957695/9809011498")
        }
        let request = GADRequest()
        interstitial.delegate = self
        interstitial.load(request)
        print(r)
    }
    
    func showAd(){
        if interstitial.isReady {
            interstitial.present(fromRootViewController: self)
        }
    }
    

}
