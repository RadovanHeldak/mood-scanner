//
//  MainController.swift
//  Mood scanner
//
//  Created by Michal Manak on 31/10/2017.
//  Copyright © 2017 Michal Manak. All rights reserved.
//

import UIKit
import GoogleMobileAds

class MainController: UIViewController, GADInterstitialDelegate {

    @IBOutlet weak var finger_view: UIView!
    @IBOutlet weak var laser_view: UIView!
    @IBOutlet weak var laser: UIImageView!
    @IBOutlet weak var laser_tint: UIView!
    @IBOutlet weak var text: UILabel!
    @IBOutlet weak var value: UILabel!
    
    var timer:Timer?
    var timer_text:Timer?
    var timer_analyzing:Timer?
    
    var moodView: MoodController!
    

    var pos_y:CGFloat?
    var dy:CGFloat?
    var movement:CGFloat = 0.665
    var counter:Int = 0
    var analyzing_counter:Int = 0
    var swipe_count:Int = 0
    var scanning_finished:Bool = false
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    

    override func viewDidLoad() {
        finger_view.layer.borderWidth = 2.5
        finger_view.layer.cornerRadius = 44
        finger_view.layer.borderColor = UIColor(red: 54.0/255.0, green: 163.0/255.0, blue: 210.0/255.0, alpha: 1).cgColor
        finger_view.clipsToBounds = true
        finger_view.layer.masksToBounds = true
        
        let gesture = UILongPressGestureRecognizer(target: self, action:  #selector (self.touch (_:)))
        gesture.minimumPressDuration = 0.1
        gesture.numberOfTouchesRequired = 1
        laser_view.addGestureRecognizer(gesture)
        laser.isHidden = true
        pos_y = laser.frame.origin.y
        laser_tint.frame.size.height = laser_view.frame.size.height + laser.frame.size.height + 10
        laser_tint.frame.origin.y = laser.frame.origin.y - laser.frame.size.height/2
        dy = movement
        text.text = "PLACE YOUR FINGER ON THE"
        value.text = "SCREEN"
    }
    
    @objc func touch(_ sender:UILongPressGestureRecognizer){
        if sender.state == .began && scanning_finished == false{
            timer = Timer.scheduledTimer(timeInterval: 0.0024, target: self, selector: #selector(self.move), userInfo: nil, repeats: true)
            counter = 0
            timer_text = Timer.scheduledTimer(timeInterval: 0.04, target: self, selector: #selector(self.count), userInfo: nil, repeats: true)
            laser.isHidden = false
            laser_tint.alpha = 0.5
            setTint()
            text.text = "SCANNING"
        }
        if sender.state == .ended && scanning_finished == false{
            timer?.invalidate()
            timer_text?.invalidate()
            reset_laser()
            text.text = "PLACE YOUR FINGER ON THE"
            value.text = "SCREEN"
        }
    }
    
    @objc func move(){
        if swipe_count == 2{
            timer?.invalidate()
            reset_laser()
        }
        
        if laser.frame.origin.y >= pos_y! + finger_view.frame.size.height{
            dy = -movement
        }
        
        if laser.frame.origin.y <= pos_y! - laser.frame.size.height{
            if dy != movement{
                swipe_count = swipe_count + 1
            }
            dy = movement
            
        }
        
        laser.frame.origin.y = laser.frame.origin.y + dy!
        setTint()
    }
    
    func setTint(){
        laser_tint.frame.origin.y = laser.frame.origin.y - laser_tint.frame.size.height +
            laser.frame.size.height/2
    }
    
    @objc func count(){
        counter = counter + 1
        value.text = String(counter) + "%"
        if counter == 100{
            timer_text?.invalidate()
            timer?.invalidate()
            reset_laser()
            scanning_finished = true
            timer_analyzing = Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(self.analyzing), userInfo: nil, repeats: true)
            text.text = "ANALYZING"
            value.text = "0 %"
            moodView = self.storyboard?.instantiateViewController(withIdentifier: "MoodController") as! MoodController
            moodView.createAndLoadInterstitial()
            }
    }
    
    @objc func analyzing(){
        analyzing_counter = analyzing_counter + 10
        value.text = String(analyzing_counter) + "%"
        if analyzing_counter == 100{
            timer_analyzing?.invalidate()
            analyzing_counter = 0
            scanning_finished = false
            self.present(moodView, animated: false, completion: nil)
        }
    }
    
    func reset_laser(){
        swipe_count = 0
        laser.isHidden = true
        laser.frame.origin.y = pos_y!
        laser_tint.alpha = 0
        dy = movement
        
    }


    @IBAction func unwind(segue:UIStoryboardSegue) {
        reset_laser()
        text.text = "PLACE YOUR FINGER ON THE"
        value.text = "SCREEN"
    }
    
   
    @IBAction func share(_ sender: Any) {

        let text = "Mood Scanner - iOS app on App Store\n\n" + "\n\n" + "#MoodScanner\n"
        
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    

    
}
